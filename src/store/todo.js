export default {
    namespaced: true,
    state: () => ({
        list: []
    }),
    mutations: {
        add(store, value) {
            store.list.push(value)
        },
        delete(store, index) {
            let list = [...store.list]
            list.splice(index, 1)
            store.list = list
        }
    },
    actions: {
        add({ commit }, value) {
            commit('add', value)
        },
        delete({ commit }, index) {
            commit('delete', index)
        }
    },
    getters: {
        list: store => store.list
    }
}
