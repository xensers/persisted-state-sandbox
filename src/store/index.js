import Vuex from "vuex";
import Vue from "vue";
import { createPersistedState } from "vuex-electron"

import todo from "@/store/todo"
import Storage from "@/storage";

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        todo
    },
})

export const enablePersistedState = (storageKey) => {
    createPersistedState({
        storageKey,
        storage: new Storage({ name: 'vuex' })
    })(store)
}

export default store
