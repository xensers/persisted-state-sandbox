import { ipcMain } from 'electron'
import MainStorage from "@/storage/store";

const instances = {}

export default function initMainStore() {
    ipcMain.on('storage', (event, opt) => {
        console.log(opt);
        const { name, action, args } = opt;

        if (action === 'init') {
            instances[name] = new MainStorage(opt)
            event.returnValue = true
        } else if (typeof instances[name][action] === 'function') {
            event.returnValue = instances[name][action](...args);
        } else {
            event.returnValue = false;
        }
    })
}
