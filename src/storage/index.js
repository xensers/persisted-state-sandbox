const { ipcRenderer } = require('electron')

class Storage {
  constructor(opt) {
    this.opt = {
      name: 'storage',
      ...opt
    };

    ipcRenderer.sendSync('storage', {
      action: 'init',
      ...this.opt
    })
  }

  get(key) {
    return this.send('get', [ key ], true);
  }

  set(key, value) {
    this.send('set', [ key, value ])
  }

  delete(key) {
    this.send('delete', [ key ])
  }

  send(action, args, sync = false) {
    return ipcRenderer[sync ? 'sendSync': 'send']( 'storage', {
      name: this.opt.name,
      action,
      args
    })
  }
}


export default Storage
