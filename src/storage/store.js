const electron = require('electron');
const path = require('path');
const fs = require('fs');

class MainStorage {
  constructor(opts) {
    this.opts = opts;
    const userDataPath = (electron.app || electron.remote.app).getPath('userData');
    this.path = path.join(userDataPath, (opts.name || 'storage') + '.json');
    this.data = parseDataFile(this.path, opts.defaults || {});
    this.writeReady = true;
    this.saveRequest = false;
  }

  get(key) {
    return this.data[key];
  }

  set(key, val) {
    this.data[key] = val;
    this.save();
  }

  delete(key) {
      delete this.data[key];
      this.save();
  }

  save() {
    if (this.writeReady) {
      this.writeReady = false
      fs.writeFile(this.path, JSON.stringify(this.data), err => {
        if (err) throw err;
        this.writeReady = true;
        console.log(this.opts.name + ': saved')
        if (this.saveRequest) {
          console.log(this.opts.name + ': save request approved');
          this.saveRequest = false;
          this.save();
        }
      });
    } else {
      console.log(this.opts.name + ': save request')
      this.saveRequest = true;
    }

  }
}

function parseDataFile(filePath, defaults) {
  try {
    return JSON.parse(fs.readFileSync(filePath));
  } catch(error) {
    return defaults;
  }
}

export default MainStorage;
